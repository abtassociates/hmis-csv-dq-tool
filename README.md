# Data Quality Tool#

### Disclaimer ###

This resource is prepared by technical assistance providers and intended only to provide guidance. The contents of this document, except when based on statutory or regulatory authority or law, do not have the force and effect of law and are not meant to bind the public in any way. This document is intended only to provide clarity to the public regarding existing requirements under the law or agency policies.

### Instructions ###

Please read the [HMIS_CSV_Data_Quality_Tool_User_Guide.pdf](https://bitbucket.org/abtassociates/hmis-csv-dq-tool/downloads/User_Guide_Data_Quality_Tool_v1_508_compliant.pdf) for exact instructions on how to use this Excel tool.

No Bitbucket account is necessary to **[download the tool and User Guide](https://bitbucket.org/abtassociates/hmis-csv-dq-tool/downloads/)**. 

### System Requirements ###

Currently, this tool is supported on Windows and Windows Excel versions 2003 and later. If you have an older version of Excel, you may need to download and install the [Microsoft Access Database Engine 2010 Redistributable](https://www.microsoft.com/en-us/download/details.aspx?id=13255).

Excel for Mac is not supported at this time, as the Microsoft Access Database Engine is not compatible with Macs.

### Issues ###

If a community experiences an issue with the tool, they can submit an AAQ to the [Ask A Question](https://www.hudexchange.info/program-support/my-question/) portal on the HUD Exchange. In Step 2 of the question submission process, select “HMIS: Homelessness Management Information Systems” from the “My question is related to” drop down list.

### Updates ###

The newly released version 1.1 of the HMIS CVS Data Quality (DQ) tool is now available for your CoC’s local use. Since the first beta version of the DQ tool was released on November 15th, 2021, the following updates and changes have been made because of communities’ feedback:

**Changes that make it easier for you to investigate errors and warnings in your local data:**

1. Column orders in the Flagged IDs window are now consistent across all the DQ Checks. 
2. DQ Check 1 and 2 pulls the most recent EntryDate for any flagged records. 
3. DQ Check 6, 7, and 8 includes ExitDate in the Flagged IDs window.
4. DQ Check 11 includes OperatingStartDate and OperatingEndDate in the Flagged IDs window.
5. DQ Check 13 includes the fields InventoryStartDate, Household Type, TotalDedicatedBeds and BedInventory in the Flagged IDs window. 
6. DQ Check 13 to DQ Check 14 relabeled to keep checks grouped together by type.

**Changes that will help you identify even more potential data quality issues:**

1. DQ Checks 13a to 13d were added to flag overlapping enrollments.

**Changes that fix an issue in the Beta version of the tool:**  

1. DQ Check 11 now only flags records where the enrollment's ExitDate or EntryDate is AFTER (rather than AFTER OR EQUAL TO) the OperatingEndDate.
2. DQ Check 11 does not flag records where the project’s OperatingEndDate was null and ExitDate was after the ExportEndDate.

**Changes that fix an issue in the 1.0 version of the tool:**  
1. Issue status icons for DQ Checks 13c and 13d now refer to the number of errors or warnings for checks DQ Checks 13c and 13d, respectively, rather than 13b.

**Changes that fix issues in the 1.1 version of the tool:**  
1. The summary status on the CSV Check Report tab now shows additional levels of nuance regarding missing files, fields, and records
2. DQ Check 11 no longer flags records where the project's OperatingEndDate is future dated. 

**Changes that fix issues in the 1.2 version of the tool:**  
1. DQ Check 8 now treats invalid move-in dates as null, in which case enrollments are defined by the period between entry and exit or report-end.

**Changes that fix issues in the 1.3 version of the tool:**  
1. DQ Check 11 had a syntax error in the calculation that is now fixed. 
2. A new check has been added after DQ Check 5. This new DQ Check 6 looks for future-dated Exit Dates.
3. Added note to what is now DQ Check 7 indicating that the check may flag some of the same records as DQ Check 6.

**Changes that fix issues in the 1.4 version of the tool:**  
1. The Export All DQ checks feature now exports all checks.

**Changes that fix issues in the 1.4.1 version of the tool:**  
1. DQ Check 6 flagged-records pop-up and the Export All DQ Checks feature work successfully now.